from django.contrib import admin, messages
from django.db import transaction
from django.http import HttpResponseRedirect

# Register your models here.
from .models import Application
from .utils import generate_api_key


def create_new_api_key(modeladmin, request, queryset):
    if not queryset.count():
        return HttpResponseRedirect(request.get_full_path())
    
    with transaction.atomic():
        for app in queryset:
            app.api_key = generate_api_key()
            app.save()

        modeladmin.message_user(request, "Generated new API keys", messages.INFO)


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = ["id", "name",'api_key']
    readonly_fields = ["api_key"]

    actions = [create_new_api_key]
