import pytest

from applicationx.models import Application
from applicationx.serializers import ApplicationSerializer


@pytest.mark.django_db
def test_application_serializer(mocker):
    data = {"name": "MyApp"}

    serializer = ApplicationSerializer(data=data)
    serializer.is_valid()
    instance = serializer.create(serializer.validated_data)
    apps = Application.objects.all()

    assert instance.name == "MyApp"
    assert apps.count() == 1
