import pytest

from applicationx.models import Application


@pytest.mark.django_db
def test_application_model(mocker):
    app = Application.objects.create(name="my")

    assert app.name == "my"
    assert app.api_key == ""
