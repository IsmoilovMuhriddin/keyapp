from secrets import token_urlsafe


def generate_api_key():
    return token_urlsafe(256)
