from django.urls import include, path

from rest_framework import routers

from .views import ApplicationViewSet, APPView

router = routers.DefaultRouter()
router.register(r"^app", ApplicationViewSet, basename="app")

urlpatterns = [
    path("", include(router.urls)),
    path("test/", APPView().as_view()),
]

