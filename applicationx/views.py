from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_202_ACCEPTED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from .models import Application
from .serializers import ApplicationSerializer
from .utils import generate_api_key


class ApplicationViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer

    @action(detail=True, methods=["POST"])
    def create_new_api_key(self, request, pk=None):
        instance = self.get_object()
        instance.api_key = generate_api_key()
        instance.save()
        return Response(self.serializer_class(instance).data, status=HTTP_202_ACCEPTED)


class APPView(APIView):
    def get(self, request):
        api_key = request.query_params.get("api_key")
        if api_key:
            apps = Application.objects.filter(api_key=api_key)
            if apps.count():
                app = apps.first()
                print(app.__dict__)
                return Response(ApplicationSerializer(instance=app).data, status=HTTP_200_OK)
            return Response({"message": "API key not found"}, status=HTTP_401_UNAUTHORIZED)
        return Response({"message": "API key not provided"}, status=HTTP_400_BAD_REQUEST)
