from rest_framework import serializers

from .models import Application
from .utils import generate_api_key


class ApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Application
        read_only_fields = ["api_key"]

    def create(self, validated_data):
        api_key = generate_api_key()
        model = self.Meta.model
        instance = model.objects.create(api_key=api_key, **validated_data)
        return instance
