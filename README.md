# KeyApp Project

## Contents
 - Introduction
 - Deployment
 - Authorization
 - How to use 
 

### Introduction
This is simple django web app that has Application model. The Model has Id, name and api key fields. Every time a new app added api key will be generated automatically. One can create a new api key for current up but cant edit it(the old api key will be deleted).

The env vars added to git repository for demonstration purposes only. Do not include env vars to version control.

### Deployment
This project is fully dockerized
You can use docker compose for deployment

To run the project on background: please run following commond from terminal from project repo.

```bash
make run
```



To stop the project 
```bash
make turnoff
```

To delete all data related to project
To stop the project 
```bash
make delete
```



### Authorization
First you need to create admin user

```bash
make createsuperuser
```

Then you can get token for this user by this request

```bash
POST http:127.0.0.1:8000/api/token/ HTTP/1.1
Content-Type: application/json
{
    "username":"<your_username_here>",
    "password":"<your_password_here>"
}
```

You will get 
```json
{
    "access":"<access_token>",
    "refresh":"<refresh_token>"
}
```

### How to use

#### Creating App


POST `api/app/` body: `{"name":"name_here"}` - for creating

PUT `api/app/<id>/` body: `{"name":"name_here"}` for editing 

DELETE `api/app/<id>/` for deleting

GET `api/app/<id>/` for viewing single app

GET `api/app/` for viewing all

example
```bash
POST http://127.0.0.1:8000/api/app/ HTTP/1.1
Authorization: Bearer <access_token>
Content-Type: application/json
{
    "name":"<new_app_name_here>",
}
```

```bash
PUT http://127.0.0.1:8000/api/app/1/ HTTP/1.1
Authorization: Bearer <access_token>
Content-Type: application/json
{
    "name":"<new_app_name_here>",
}
```



For creating new api key 
```
POST api/app/<id>/create_new_api_key/
Authorization: Bearer <access_token>
```


After getting api_key you can use this endpoint

```
GET http://127.0.0.1:8000/api/test/?api_key=<api_key>
```

You will get json data of that key's app 

### Improvements:
- Paginotion was not added for simplicity and clean view of the app, it can be added easily

- Requirements splitted by base and dev .txt
- nginx for reverse proxy
- There is also django admin action for creating api new api key from admin

