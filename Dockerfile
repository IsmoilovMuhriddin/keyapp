FROM python:3.7.4-slim

WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


RUN apt-get update && \
    apt-get -y install netcat && \
    apt-get clean


RUN pip install --upgrade pip

COPY . /usr/src/app/

RUN pip install -r requirements/base.txt
# run entrypoint.prod.sh
ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
