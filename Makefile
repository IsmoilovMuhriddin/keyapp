COMMAND = docker-compose exec web

createsuperuser:
	$(COMMAND) python manage.py createsuperuser

run:
	docker-compose up -d

turnoff:
	docker-compose down

delete:
	docker-compose down -v
